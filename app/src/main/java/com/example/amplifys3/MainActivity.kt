package com.example.amplifys3

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import com.amplifyframework.core.Amplify
import com.amplifyframework.storage.options.StorageUploadFileOptions
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.util.*

class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    private lateinit var uploadBtn: Button
    private lateinit var addBtn: ImageButton
    private lateinit var filenameTxt: TextView
    private lateinit var selectedVideoFile: File
    private lateinit var progressView: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        filenameTxt = findViewById(R.id.textView_filename)
        uploadBtn = findViewById(R.id.button_upload)
        uploadBtn.setOnClickListener {
            if (this::selectedVideoFile.isInitialized) {
                beginUpload(selectedVideoFile)
            } else {
                toast(this, "You havent selected a video file yet")
            }
        }

        addBtn = findViewById(R.id.image_plus)
        addBtn.setOnClickListener { requestOpenGallery() }

        setupProgressDialog()
    }

    private fun beginUpload(file: File) {
        progressView.show()

        BufferedWriter(FileWriter(file)).use {
            it.append("sample file contents")
        }

        val key = UUID.randomUUID().toString()
        val options = StorageUploadFileOptions.defaultInstance()
        Amplify.Storage.uploadFile(
            key,
            file,
            options,
            { transferProgress ->
                Log.i(TAG, "current: ${transferProgress.currentBytes}")
                Log.i(TAG, "total: ${transferProgress.totalBytes}")
                Log.i(TAG, "fractionCompleted: ${transferProgress.fractionCompleted}")
            },
            { result ->
                toast(this, "Upload Success: ${result.key}")
                Log.d(TAG, "Upload Success")
                Log.d(TAG, "  uuid: $key")
                Log.d(TAG, "result: ${result.key}")
                progressView.dismiss()
            },
            { err ->
                Log.e(TAG, "Upload Error", err)
                progressView.dismiss()
            })
    }

    private fun setupProgressDialog() {
        progressView = ProgressDialog(this).apply {
            isIndeterminate = true
            setCancelable(false)
            setMessage("Preparing...")
        }
    }

    private fun openGalleryGranted() {
        val intent = Intent(this, FilePickerActivity::class.java)
        intent.putExtra(FilePickerActivity.CONFIGS, Configurations.Builder().apply {
            setCheckPermission(true)
            setShowImages(false)
            setShowVideos(true)
            setSingleChoiceMode(true)
            setSkipZeroSizeFiles(true)
        }.build())
        startActivityForResult(intent, RC_VIDEO_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_VIDEO_FILE &&
            resultCode == Activity.RESULT_OK &&
            data != null
        ) {
            val files = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            files?.let { f ->
                if (f.isNotEmpty()) {
                    Log.d(TAG, "onActivityResult: $f")
                    Log.d(TAG, "uri: ${f[0].uri}")
                    Log.d(TAG, "path: ${f[0].path}")
                    Log.d(TAG, "bucketId: ${f[0].bucketId}")
                    Log.d(TAG, "bucketName: ${f[0].bucketName}")
                    Log.d(TAG, "date: ${f[0].date}")
                    Log.d(TAG, "duration: ${f[0].duration}")
                    Log.d(TAG, "height: ${f[0].height}")
                    Log.d(TAG, "width: ${f[0].width}")
                    Log.d(TAG, "id: ${f[0].id}")
                    Log.d(TAG, "mediaType: ${f[0].mediaType}")
                    Log.d(TAG, "mimeType: ${f[0].mimeType}")
                    Log.d(TAG, "thumbnail: ${f[0].thumbnail}")

                    val file = UriUtil.readContentToFile(this, f[0].uri)
                    file?.let {
                        selectedVideoFile = it
                        filenameTxt.text = it.name
                    }
                }
            }
        }
    }

    @AfterPermissionGranted(RC_READ_EXTERNAL)
    private fun requestOpenGallery() {
        val perms = Manifest.permission.READ_EXTERNAL_STORAGE
        if (EasyPermissions.hasPermissions(this, perms)) {
            openGalleryGranted()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "App needs read permission",
                RC_READ_EXTERNAL,
                perms
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        Log.d(TAG, "onPermissionsGranted: $requestCode")
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        Log.d(TAG, "onPermissionsDenied: $requestCode")
    }

    companion object {
        private const val RC_READ_EXTERNAL = 42

        private const val RC_VIDEO_FILE = 64

        private const val TAG = "MainActivity"
    }
}