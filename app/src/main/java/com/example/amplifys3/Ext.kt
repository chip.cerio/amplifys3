package com.example.amplifys3

import android.app.Activity
import android.content.Context
import android.widget.Toast

fun Activity.toast(ctx: Context, msg: String) {
    Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
}